﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Move : MonoBehaviour
{

    
    private Rigidbody2D rbitem;

    public float speed = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        rbitem = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (transform.rotation.eulerAngles.y < 80) {
        //    transform.Rotate(new Vector3(0, 35, 0) * Time.deltaTime);
        //}
        //else if(transform.rotation.eulerAngles.y > -80)
        //{
        //    transform.Rotate(new Vector3(0, -35, 0) * Time.deltaTime);
        //}
        
        float moveHorizontal = 0;
        float moveVertical = -0.5f;
        Vector2 move = new Vector2(moveHorizontal, moveVertical);
        rbitem.AddForce(move * speed);
    }
}
